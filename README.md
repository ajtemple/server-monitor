# Server Monitor

A simple API and web page to monitor the game servers on my home server. The application is split into two parts:

## Server

An API written using Express.JS, running on the server itself. Can either be run in a Docker container using the supplied Dockerfile or using a tool such as PM2.

## Client

A simple web page written using React, Bootstrap Grid and Axios. Production build generates a single `index.html` file with bundled scripts.
