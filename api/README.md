# Server

An API written using NodeJS (Express) to query game servers and display information about the system.

Can be run using a tool such as PM2/Supervisor, or using the included Dockerfile.

## SSL

To enable SSL, set `ssl.enabled` in conf.json to true, and supply a key & cert file. Required if the client is using SSL.

## Endpoints

`/api/ping` - Returns the version of the API if the query is successful. Used to test whether the API is running.

`/api/servers/:type/:host/:port?` - Queries a gameserver using the type, host and optional port parameters supplied. See Gamedig documentation for more information on each of these parameters.

`/api/system/info` - Grabs information such as OS, CPU, total memory from the host system.

`/api/system/cpu` - Grabs CPU usage on the host system (% utilisation over 1 second).

`/api/system/memory` - Grabs the free, used and total memory available on the system.
