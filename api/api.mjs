import express from "express";
import cors from "cors";
import helmet from "helmet";
import morgan from "morgan";
import Gamedig from "gamedig";
import os from "os";
import osu from "node-os-utils";
import fs from "fs";
import https from 'https';

// CONFIG
const app = express();                                // Initialise Express app
app.set("port", process.env.PORT || 3031);            // Listen on port 3031 if none defined in env

app.use(helmet());                                    // set HTTP headers with Helmet
app.use(express.json());                              // add support for JSON encoded request bodies
app.use(cors());                                      // enable cross-site support
app.use(morgan("tiny"));                              // use Morgan for HTTP logging

const config = JSON.parse(fs.readFileSync("conf.json", "utf-8")); // load config file

// ROUTES

// Ping - used to test the connection to the API on website load
app.get("/api/ping", (req, res) => {
  res.json({
    "status": "success",
    "version": config.version
  });
});

// Query a gameserver with supplied variables:
// type - which type of gameserver we're querying - see Gamedig docs for more info
// host - IP/host of the server
// port - Query port for the server
app.get("/api/servers/:type/:host/:port?", (req, res) => {
  const queryParams = {
    type: req.params.type,
    host: req.params.host,
    ...(!!req.params.port && {
      port: req.params.port
    })
  };

  Gamedig.query(queryParams)
    .then((state) => { // if query is successful

      res.json({ status: state });

    }).catch((err) => {  // if query is unsuccessful

      res.json({ status: "offline" });

    });
});

// Grab system information of the host machine.
app.get("/api/system/info", (req, res) => {
  // Return info about the system (OS, kernel, etc)
  const cpuModel = osu.cpu.model();     // CPU model
  const totalMem = osu.mem.totalMem();  // Total installed RAM
  const platform = osu.os.type();       // OS platform (Linux, Windows, etc)
  const arch = osu.os.arch();           // OS architecture (x86/64, ARM, etc)
  const kernel = os.release();          // OS kernel version

  osu.os.oos().then((operatingSys) => { // OS name - uses a promise for some reason.
    res.json({
      OS: `${operatingSys} ${platform}`,
      cpuModel,
      totalMem,
      arch,
      kernel,
    });
  });
});

// Grabs total, used and free memory on the host machine
app.get("/api/system/memory", (req, res) => {
  osu.mem.info().then((info) => {
    res.json({
      totalMem: info.totalMemMb,
      usedMem: info.usedMemMb,
      freeMem: info.freeMemMb,
      freePercent: info.freeMemPercentage,
    });
  });
});

// Grab CPU usage on host machine
app.get("/api/system/cpu", (req, res) => {
  osu.cpu.usage().then((usage) => {   // usage() grabs average CPU usage over a 1 second period
    res.json({
      cpu_usage: usage
    });
  });
});

//  Start server

// SSL ENABLED
if (config.ssl.enabled) {
  const key = fs.readFileSync(config.ssl.key, 'utf-8');
  const cert = fs.readFileSync(config.ssl.cert, 'utf-8');

  const credentials = {
    key,
    cert
  };

  const httpsServer = https.createServer(credentials, app); // create HTTPS server using Express app & SSL credentials
  httpsServer.listen(app.get("port"), () => {
    console.log(`API listening on port ${app.get("port")} [SSL enabled]`);
  });
}
// SSL DISABLED
else {
  app.listen(app.get("port"), () => {
    console.log(`API listening on port ${app.get("port")}`);
  })
}