# Client

A front-end app to interface with the Express API backend. Bootstrapped using [Create React App](https://create-react-app.dev/).

## Usage

### `config.json`

Set the `apiUrl` variable to point to where your API is hosted. Mixing HTTP and HTTPS is not possible - if the client is using SSL, the API must as well and vice versa.

Add each server to be queried into the `servers` section:

* `name` String - The name of the server. Can be set to anything, this just sets the title.
* `image` String - The image to display in the server block. Looks for the filename inside `public/images/servers`.
* `query` Object:
  * `type` String - The game ID or query protocol to use - see [GameDig](https://github.com/gamedig/node-gamedig) for details.
  * `host` String - Hostname or IP of the server
  * `port` Int (optional) - Query port of the server. Helpful if running multiple instances of the same server, or if the server uses a non-standard port for the query protocol
* `tags` Array - a list of optional tags to include in the server details. These differ per game and are included in the `raw` section of the response. (Example - Mordhau returns a tag with the value `N:[Gamemode]`)
  * `tag` String - The tag to include (the part before the colon. "N" in the example above)
  * `label` String - The label to display this tag with

### `Images`

The app is built around images with a resolution of 272x380. It will likely work with different resolutions, but will not display correctly. The important thing is to use images in a portrait aspect ratio (as they are scaled based on width), and to use the same ratio for each image to ensure consistency.

## Building

Once the options in `config.json` are set, the app can be built for production by running either `npm run build` or `yarn build`, depending on your package manager. This will create a static HTML file inside the `build` folder which can be hosted however you want. See [the CRA docs](https://create-react-app.dev/docs/) for more information on available scripts and deployment.
