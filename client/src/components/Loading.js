import './Loading.scss';

const Loading = props => {
  const bg = {backgroundColor: props.colour};
  return (
    <div className="Loading">
      <div className="bar1" style={bg}></div>
      <div className="bar2" style={bg}></div>
      <div className="bar3" style={bg}></div>
      <div className="bar4" style={bg}></div>
    </div>
  )
}

export default Loading;