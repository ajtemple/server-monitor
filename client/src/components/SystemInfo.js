import { useState, useEffect } from "react";
import axios from "axios";

import ProgressBar from "./ProgressBar";
import Loading from "./Loading";

import * as config from "../config.json";

import "./SystemInfo.scss";

const apiUrl = config.apiUrl;                       // Get API URL from config file

const SystemInfo = () => {

  // CPU USAGE
  const [cpuUsage, setCpuUsage] = useState(null);   // initialise state
  useEffect(() => {                                 // update state and set update interval on component mount
    updateCpu();
    const cpuInterval = setInterval(updateCpu, 5000);
    return () => clearInterval(cpuInterval);        // clear interval on unmount
  }, []);

  // RAM USAGE
  const [ramUsage, setRamUsage] = useState(null);   // initialise state
  useEffect(() => {                                 // update state and set update interval on component mount
    updateMem();
    const memInterval = setInterval(updateMem, 5000);
    return () => clearInterval(memInterval);        // clear interval on unmount
  }, []);

  // SYSTEM INFO
  const [systemInfo, setSystemInfo] = useState(null);
  useEffect(() => {                                 // update state on component mount
    axios.get(`${apiUrl}/api/system/info`)   // push system info to state on success
      .then((res) => setSystemInfo(res.data))       // log error to console on failure
      .catch((err) => console.log(err));
  }, []);

  // UPDATE FUNCTIONS
  const updateCpu = () => {
    axios.get(`${apiUrl}/api/system/cpu`)    // query CPU usage endpoint
      .then((res) => setCpuUsage(res.data))         // push usage to state on success
      .catch((err) => setCpuUsage(err));            // push error to state on failure
  }

  const updateMem = () => {
    axios.get(`${apiUrl}/api/system/memory`) // query memory usage endpoint
      .then((res) => setRamUsage(res.data))         // push usage to state on success
      .catch((err) => setRamUsage(err));            // push error to state on failure
  }

  // RENDER
  return (
    <div className="row mb-4 mb-md-2">
      <div className="col-md-6 px-2 mb-3 my-md-0">
        <div className="SystemInfo pb-2">
          <h3>System Information</h3>
          {
            systemInfo === null
              ? <Loading colour="#333" />
              : (
                <p>
                  OS: <span>{systemInfo.OS} ({systemInfo.arch})</span> <br />
                  Kernel: <span>{systemInfo.kernel}</span> <br />
                  CPU: <span>{systemInfo.cpuModel}</span> <br />
                  Total Memory: <span>{(systemInfo.totalMem / 1024 ** 3).toFixed(2)} GB</span>
                </p>
              )
          }
        </div>
      </div>
      <div className="col-md-6 px-2">
        <div className="SystemUsage pb-2">
          <h3>System Usage</h3>
          {
            cpuUsage !== null
              ? <ProgressBar usage={cpuUsage.cpu_usage} label={`CPU Usage: ${cpuUsage.cpu_usage}%`} />
              : <Loading colour="#333" />
          }

          {
            ramUsage !== null
              ? <ProgressBar usage={100 - ramUsage.freePercent} label={`RAM Usage: ${(ramUsage.usedMem / 1024).toFixed(2)} / ${(ramUsage.totalMem / 1024).toFixed(2)}GB`} />
              : <Loading colour="#333" />
          }
        </div>
      </div>
    </div>
  );
};

export default SystemInfo;
