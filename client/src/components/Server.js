import { useEffect, useState } from "react";
import axios from "axios";

// Import components
import Loading from "./Loading";

// Import config
import * as config from '../config.json';

// Import SVGs
import { ReactComponent as VacIcon } from '../assets/images/icons/vac.svg';
import { ReactComponent as PwdIcon } from '../assets/images/icons/password.svg';

// Import SASS
import './Server.scss';
const apiUrl = config.apiUrl;

const Server = props => {



  const [serverStatus, setServerStatus] = useState(null);                                           // initialise server state to null
  useEffect(() => {   
    const queryServer = () => {                                                                     // server query method - defined in useEffect to prevent exhaustive deps warning.
      let qString = `${props.info.query.type}/${props.info.query.host}`;                            // set query string to type/host - API endpoint = /api/servers/:type/:host/:port?
      if (!!props.info.query.port) qString += `/${props.info.query.port}`;                          // append port to query string if set
  
      axios.get(`${apiUrl}/api/servers/${qString}`)                                          // query the server API
        .then(res => {
          setServerStatus(res.data);                                                                // set status to state on success
        })
        .catch(err => {
          setServerStatus(err);                                                                     // set error to state on failure
        })
    }          
                                                                                                    // update state and start update interval
    queryServer();
    const queryInterval = setInterval(queryServer, 60000);                                          // queries the server every 60 seconds
    return () => clearInterval(queryInterval);                                                      // clear interval on component unmount
  }, [props]);

  // creates list of players from the object returned by the API
  const getPlayerList = () => {
    const players = serverStatus.status.players;                                                    // get raw player list: { "name": "username", "raw": {} }
    var playerList = [];                                                                            // make empty array to push names to                                                                       

    players.forEach(player => {
      const {name} = player;                                                                        // destructure player object to just pull username
      if (name !== "") {                                                                            // check if username field is empty, as some games don't populate this until the player has finished joining
        playerList.push(name);                                                                      // add username to playerList array
      }
    });

    return (playerList.length > 0)                                                                  // check if playerList is empty
      ? <p>Player list: <span>{playerList.join(", ")}</span></p>                                    // if not empty, return formatted list
      : null                                                                                        // if empty, return null
  }

  // method to render the query result when it is loaded into state
  // this code is a bit of an abomination.
  const renderDetails = () => {
    const isOnline = !(serverStatus.status === "offline");                                          // check whether the server is online or not
    const tags = props.info.tags || null;                                                           // check whether any optional tags are set in config.json, if not set to null

    return (
      <div className="Server">
        <div className="ServerImage">
          <img
            src={`${process.env.PUBLIC_URL}/images/servers/${props.info.image}`}                    // grabs image name from config.json and loads file from %PUBLIC_URL%/images/servers
            alt={props.info.name}
            title={props.info.name}                                                                 // set alt and title to the server name
          />
        </div>
        <div className="ServerStatus">
          {
            isOnline &&                                                                             // section only renders if server is online
            <div className="ServerIcons">
              <VacIcon
                className={serverStatus.status.raw.secure === 1 ? 'enabled' : 'disabled'}           // sets class & title based on whether the server is VAC enabled
                title={`VAC: ${serverStatus.status.raw.secure === 1 ? 'enabled' : 'disabled'}`}
              />
              <PwdIcon
                className={serverStatus.status.password ? 'enabled' : 'disabled'}                   // same as above but with password.
                title={`Password: ${serverStatus.status.password ? 'enabled' : 'disabled'}`}
              />
            </div>
          }
          <h5>{props.info.name}</h5>
          <p>Status: 
            {
              isOnline                                                                              // sets text and class based on online status
                ? <span className="online"> online</span>
                : <span className="offline"> offline</span>
            }
          </p>
          {
            isOnline &&                                                                             // section only renders if server is online - formats the server details
            <>
              <p>Server name: <span>{serverStatus.status.name}</span></p>
              <p>Map: <span>{serverStatus.status.map}</span></p>
              <p>Players: <span>
                {
                  `${serverStatus.status.players.length} / ${serverStatus.status.maxplayers}`       // sets player count string, e.g: '0 / 15'
                }
              </span></p>
              {
                serverStatus.status.players.length > 0 && getPlayerList()
              }
              {
                tags !== null &&                                                                    // section only renders if tags variable is not null
                tags.map((tag, index) => {                                                          // loop through tags
                  const rawTags = serverStatus.status.raw.tags;                                     // grab list of tags from query response - in TAG:VALUE format (e.g. gamemode for Mordhau = N:Deathmatch)
                  var el;                                                                           // initialise empty variable for return value
                  rawTags.forEach(i => {                                                            // loop through raw tags
                    const s = i.split(':');                                                         // split each tag using : as delim.
                    if (s[0] === tag.tag) {                                                         // check if the first part matches the tag in config.json
                      el = <p key={index}>{tag.label}: <span>{s[1]}</span></p>;                     // set el to the HTML element to render
                    }
                  })
                  return el;
                })
              }
              <p>Address: <span>{serverStatus.status.connect}</span></p>
              <p>Ping: <span>{serverStatus.status.ping}ms</span></p>
            </>
          }
        </div>
      </div>
    )
  }

  return (
    <div className="col-12 col-md-6 mb-4">
      {
        serverStatus === null                                                                       // displays loading animation if state is null
          ? <div className="Server"><Loading colour="#ffaa65" /></div>
          : renderDetails()                                                                         // otherwise calls render method
      }
    </div>
  )
}

export default Server;