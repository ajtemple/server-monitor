import './ProgressBar.scss';

const ProgressBar = (props) => {
  const barColour = () => {
    if (props.usage <= 20) {
      return "#2dd4bf";
    }
    else if (props.usage <= 40) {
      return "#4ade80";
    }
    else if (props.usage <= 60) {
      return "#facc15";
    }
    else if (props.usage <= 80) {
      return "#fb923c";
    }
    else {
      return "#f87171";
    }
  }

  return (
    <div className="ProgressBar">
      <p>{props.label}</p>
      <span style={{width: props.usage+"%", backgroundColor: barColour()}}></span>
    </div>
  )
}

export default ProgressBar;