import Server from './Server';

import './ServerList.scss';

import * as config from '../config.json';
const servers = config.servers;

const ServerList = () => {
  return (
    <div className="row">
      <div className="col px-2">
        <div className="ServerList">
          <div className="row p-3">
            {servers.map((server, index) => {
              return <Server info={server} key={index}/>
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ServerList;