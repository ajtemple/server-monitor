import SystemInfo from "./SystemInfo";
import ServerList from "./ServerList";

import './ServerDetails.scss';

const ServerDetails = () => {
    return (
        <div className="container px-0">
            <SystemInfo />
            <ServerList/>
        </div>
    )
}

export default ServerDetails;