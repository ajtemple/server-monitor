import { useEffect, useState } from "react";
import axios from "axios";
import ServerDetails from './components/ServerDetails';

import './App.scss';

import * as config from './config.json';
const apiUrl = config.apiUrl;

const App = () => {

  const [apiStatus, setApiStatus] = useState("disconnected");

  useEffect(() => {
    axios.get(`${apiUrl}/api/ping`)
    .then(state => setApiStatus(state.data))
    .catch(err => setApiStatus("disconnected"))
  }, []);

  const apiStatusElement = () => {
    switch (apiStatus) {
      case undefined:
      case "disconnected":
        return <span className="api-disconnected">Disconnected</span>;
      default:
        return <span className="api-connected">Connected</span>;
    }
  }

  return (
    <div className="App">
      <div className="header">
        <div className="container px-0">
          <div className="row">
            <div className="header-title col-6"><h4>Server Monitor</h4></div>
            <div className="api-status col-6"><p>API Status: {apiStatusElement()}</p></div>
          </div>
        </div>
      </div>
      <div className="body">
        
            {(apiStatus === undefined || apiStatus === "disconnected") 
            ? <div className="container px-0"><p>API is not connected.</p></div>
            : <ServerDetails />}
        
      </div>
    </div>
  );
}

export default App;
